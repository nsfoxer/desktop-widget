# Desktop Widget

[toc]

​	这是一个针对`wayfire`窗口管理器实现的桌面天气组件。它使用`[gtk-layer-shell](https://github.com/wmww/gtk-layer-shell)`实现。



## 支持桌面

只在`wayland`上可以运行。

- 基于`wlroots`的桌面。如：`sway`、`wayfire`
- 实现`zwlr_layer_shell_v1`的桌面。

## 截图

![image-20220820200734355](https://nsfoxer-oss.oss-cn-beijing.aliyuncs.com/img/92b7377a613dbb23e82214689e9c0d50.webp)
