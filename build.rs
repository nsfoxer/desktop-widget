use gtk::gio;

fn main() {
    println!("Start Build");
    gio::compile_resources("resources", "resources/resources.gresource.xml", "compiled.gresource");
    println!("End Build");
}
