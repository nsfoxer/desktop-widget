use chrono::{Datelike, Timelike};
use curl::easy::Easy;
use flate2::read::GzDecoder;
use serde::Deserialize;
use serde_aux::prelude::*;

use crate::utils::network::get_http_as_json;
use crate::utils::network::get_http_as_u8;

static WEEKDAY: [&str; 7] = [
    "星期一",
    "星期二",
    "星期三",
    "星期四",
    "星期五",
    "星期六",
    "星期日",
];
static WEEKDAY2: [&str; 7] = ["周一", "周二", "周三", "周四", "周五", "周六", "周日"];

#[derive(Debug, Default)]
pub struct LocationData {
    pub success: bool,
    pub ip_info: IPInfo,
    pub location: String,
    pub city_code: CityCode,
    pub now_temp: NowTemp,
    pub day_temp: DayTemp,
    pub time: Time,
}

impl LocationData {
    pub fn update_data(&mut self) -> bool {
        self.update_time();
        if self.refresh_ip_info()
            && self.get_city_code()
            && self.get_now_temp()
            && self.get_day_temp()
        {
            self.success = true;
        } else {
            self.success = false;
        }

        return self.success;
    }

    fn update_time(&mut self) {
        let now = chrono::Local::now();
        self.time.night = true;
        if now.hour() >= 6 && now.hour() <= 17 {
            self.time.night = false;
        }
        let day_num = now.weekday().num_days_from_monday();
        self.time.now_week = WEEKDAY[day_num as usize].to_string();

        self.time.day1_week = WEEKDAY2[day_num as usize].to_string();
        self.time.day2_week = WEEKDAY2[((day_num + 1) % 7) as usize].to_string();
        self.time.day3_week = WEEKDAY2[((day_num + 2) % 7) as usize].to_string();
        self.time.update_time = format!("{:02}:{:02}", now.hour(), now.minute());
    }

    fn refresh_ip_info(&mut self) -> bool {
        let ip_url = "http://myip.ipip.net/json";

        let mut ip_info: Option<IPInfo> = None;
        let mut count = 0;
        while count < 3 {
            count += 1;
            match get_http_as_json(ip_url) {
                Ok(s) => {
                    ip_info = Some(s);
                    break;
                }
                Err(e) => {
                    log::warn!("Get IPInfo Error: {}", e.to_string());
                }
            }
        }
        match ip_info {
            Some(mut ip_info) => {
                ip_info.data.location.reverse();
                log::info!("{:?}", ip_info);
                self.ip_info = ip_info;

                for l in self.ip_info.data.location.iter().skip(1) {
                    if !l.is_empty() {
                        self.location = l.clone();
                        break;
                    }
                }
                return true;
            }
            None => {
                log::error!("Get IPInfo Error");
                return false;
            }
        }
    }

    fn get_city_code(&mut self) -> bool {
        let location = Easy::new().url_encode(self.location.as_bytes());
        let url = format!("https://geoapi.qweather.com/v2/city/lookup?location={}&key=cd47a6e3b16a43c09d8aefdc19686585", location);
        log::info!("{}", url);
        if let Ok(data) = get_http_as_u8(url.as_str()) {
            let data = GzDecoder::new(data.as_slice());
            self.city_code = serde_json::from_reader(data).unwrap();
            log::info!("{:?}", self.city_code);
        } else {
            log::error!("Get City Code Failed!");
            return false;
        }
        return true;
    }

    fn get_now_temp(&mut self) -> bool {
        let url = format!("https://devapi.qweather.com/v7/weather/now?location={}&key=cd47a6e3b16a43c09d8aefdc19686585", self.city_code.location[0].id);
        log::info!("{}", url);
        match get_http_as_u8(url.as_str()) {
            Ok(data) => {
                let mut data = GzDecoder::new(data.as_slice());
                self.now_temp = serde_json::from_reader(data).unwrap();
                log::info!("{:?}", self.now_temp);
                return true;
            }
            Err(e) => {
                log::error!("Get Now Temp error: {}", e.to_string());
                return false;
            }
        }
    }

    fn get_day_temp(&mut self) -> bool {
        let url = format!("https://devapi.qweather.com/v7/weather/3d?location={}&key=cd47a6e3b16a43c09d8aefdc19686585", self.city_code.location[0].id);
        log::info!("{}", url);
        match get_http_as_u8(url.as_str()) {
            Ok(data) => {
                let data = GzDecoder::new(data.as_slice());
                self.day_temp = serde_json::from_reader(data).unwrap();
                log::info!("{:?}", self.day_temp);
                return true;
            }
            Err(e) => {
                log::error!("Get day tempature error: {}", e.to_string());
                return false;
            }
        }
    }
}

#[derive(Debug, Deserialize, Default)]
pub struct IPInfo {
    ret: String,
    pub data: IPInfoData,
}

#[derive(Debug, Deserialize, Default)]
pub struct IPInfoData {
    pub ip: String,
    pub location: Vec<String>,
}

#[derive(Debug, Deserialize, Default)]
pub struct CityCode {
    code: String,
    pub location: Vec<CityCodeLocation>,
}

#[derive(Debug, Deserialize, Default)]
pub struct CityCodeLocation {
    pub name: String,
    pub id: String,
}

#[derive(Debug, Deserialize, Default)]
pub struct NowTemp {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    code: u16,
    pub now: NowTempData,
}

#[derive(Debug, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct NowTempData {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub temp: i8,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub feels_like: i8,
    pub text: String,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub icon: u16,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub humidity: u8,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub precip: f64,
}

#[derive(Debug, Deserialize, Default)]
pub struct DayTemp {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    code: u16,
    pub daily: Vec<DayTempData>,
}

#[derive(Debug, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct DayTempData {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub temp_max: i8,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub temp_min: i8,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub icon_day: u16,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub icon_night: u16,
}

#[derive(Debug, Default)]
pub struct Time {
    pub night: bool,
    pub now_week: String,
    pub day1_week: String,
    pub day2_week: String,
    pub day3_week: String,
    pub update_time: String,
}
