use glib::subclass::prelude::*;
use gtk::{glib, prelude:: *};
use std::cell::RefCell;

#[derive(Default)]
pub struct RowData {
    name: RefCell<Option<String>>,
}



#[glib::object_subclass]
impl ObjectSubclass for RowData {
    const NAME: &'static str = "RowData";
    type Type = super::RowData;
    type ParentType = glib::Object;
}

impl ObjectImpl for RowData {
    fn properties() -> &'static [glib::ParamSpec] {
        use once_cell::sync::Lazy;
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![
                glib::ParamSpecString::new(
                    "name", "Name", "Name", None, glib::ParamFlags::READWRITE)
            ]
        });
        PROPERTIES.as_ref()
    }

    fn set_property(&self, _obj: &Self::Type, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
        match pspec.name() {
            "name" => {
                let name = value.get().expect("type conformity checked by `Object::set_property`");
                self.name.replace(name);
            },
            _ => unimplemented!()
        }
    }

    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "name" => self.name.borrow().to_value(),
            _ => unimplemented!()
        }
    }
}
