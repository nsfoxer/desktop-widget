mod imp;

use gtk::glib;

glib::wrapper! {
    pub struct RowData(ObjectSubclass<imp::RowData>);
}

impl RowData {
    pub fn new(name: &str) -> RowData {
        glib::Object::new(&[("name", &name)]).expect("Failed to create row data")
    }
}
