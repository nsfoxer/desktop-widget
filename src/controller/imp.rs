use std::collections::HashMap;
use std::thread;

use glib::{Continue, MainContext, ObjectExt, Receiver, PRIORITY_DEFAULT};
use gtk::gdk_pixbuf::Pixbuf;
use gtk::traits::{ImageExt, ButtonExt};
use gtk::{prelude::BuilderExtManual, Builder, Button, Image, Label};
use log;

use crate::model::imp::LocationData;
use crate::row_data::RowData;

type SetView = fn(&LocationData) -> String;

pub struct LocationController {
    builder: Builder,
    label_data_map: HashMap<String, (RowData, SetView)>,
    image_data_map: HashMap<Image, (u8, SetView)>,
    button_data_map: HashMap<Button, ()>,
}

impl LocationController {
    pub fn new(builer: Builder) -> Self {
        let s = Self {
            builder: builer,
            label_data_map: Default::default(),
            image_data_map: Default::default(),
            button_data_map: Default::default(),
        };
        s
    }

    pub fn add_label(&mut self, id: &str, func: SetView) {
        let rdata = RowData::new("Loading");
        Self::bind_label(&rdata, &self.builder.object::<Label>(id).unwrap());
        self.label_data_map.insert(id.to_string(), (rdata, func));
    }

    pub fn add_image(&mut self, id: &str, size: u8, func: SetView) {
        let image = self.builder.object(id).unwrap();
        self.image_data_map.insert(image, (size, func));
    }

    pub fn add_butoon(&mut self, id: &str, description: &str) {
    }

    pub fn update_all(&mut self) {
        let (sender, receiver) = MainContext::channel(PRIORITY_DEFAULT);
        thread::spawn(move || {
            log::info!("Start Send Data");
            let mut m = LocationData::default();
            if !m.update_data() {
                sender.send(None).unwrap();
                log::error!("Update Data Failed");
            } else {
                sender.send(Some(m)).unwrap();
            }
            log::info!("End Send Data");
        });
        self.recevie_data(receiver);
    }

    fn recevie_data(&mut self, receiver: Receiver<Option<LocationData>>) {
        //let location = self.location.clone();
        let mut s = HashMap::new();
        for (n, (d, f)) in self.label_data_map.iter() {
            s.insert(n.clone(), (d.to_owned(), f.to_owned()));
        }

        let mut images = HashMap::new();
        for (n, (s, f)) in self.image_data_map.iter() {
            images.insert(n.clone(), (s.to_owned(), f.to_owned()));
        }

        receiver.attach(None, move |model| {
            let model = match model {
                Some(m) => m,
                None => {
                    log::warn!("Skip None Data");
                    return Continue(true);
                }
            };
            log::info!("Start Receve Data");
            for (n, (d, f)) in s.iter() {
                log::info!("Id: {} Value: {}", n, f(&model));
                d.set_property("name", f(&model));
            }

            for (n, (s, f)) in images.iter() {
                let pxibuf =
                    Pixbuf::from_resource_at_scale(f(&model).as_str(), *s as i32, *s as i32, false).unwrap();
                n.set_pixbuf(Some(&pxibuf));
            }
            log::info!("End Receve Data");
            Continue(true)
        });
    }

    fn bind_label(data: &RowData, label: &Label) {
        data.bind_property("name", label, "label")
            .flags(
                glib::BindingFlags::DEFAULT
                    | glib::BindingFlags::SYNC_CREATE
                    | glib::BindingFlags::BIDIRECTIONAL,
            )
            .build();
    }
}
