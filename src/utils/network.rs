use curl::easy::Easy;
use curl::easy::Handler;
use serde::de;

use std::error::Error;

struct Collector(Vec<u8>);

impl Handler for Collector {
    fn write(&mut self, data: &[u8]) -> Result<usize, curl::easy::WriteError> {
        self.0.extend_from_slice(data);
        Ok(data.len())
    }
}

pub fn get_http_as_string(url: &str) -> Result<String, Box<dyn Error>> {
    let data = get_http_as_u8(url)?;
    let data = String::from_utf8(data)?;

    Ok(data)
}

pub fn get_http_as_json<T>(url: &str) -> Result<T, Box<dyn Error>>
where
    T: de::DeserializeOwned,
{
    let data = get_http_as_u8(url)?;
    let r = serde_json::from_slice(&data)?;

    Ok(r)
}

pub fn get_http_as_u8(url: &str) -> Result<Vec<u8>, Box<dyn Error>> {
    let mut easy = Easy::new();
    let mut data = Vec::new();
    easy.get(true)?;
    easy.url(url)?;

    {
        let mut transfer = easy.transfer();
        transfer.write_function(|ndata| {
            data.extend_from_slice(ndata);
            Ok(ndata.len())
        })?;
        transfer.perform()?;
    }

    Ok(data)
}
