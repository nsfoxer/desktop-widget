mod controller;
mod model;
mod row_data;
mod utils;

use std::rc::Rc;
use std::cell::RefCell;

use controller::imp::LocationController;
use env_logger::{self, Env};
use glib::timeout_future_seconds;
use gtk::prelude::*;
use gtk::{self, Application, Builder};
use gtk::{cairo, gdk};
use gtk_layer_shell;

static RESOURCE_PREFIX: &'static str = "/org/gtk_rs/rdesk";

fn main() {
    gio::resources_register_include!("compiled.gresource").expect("Failed to register resources.");
    let env = Env::default().filter_or("MY_LOG_LEVEL", "error");
    env_logger::init_from_env(env);
    let app = gtk::Application::new(Some("com.test.aopp"), Default::default());

    app.connect_activate(build_ui);

    app.run();
}

fn build_ui(app: &Application) {
    let builder = Builder::from_resource(format!("{}/main.glade", RESOURCE_PREFIX).as_str());
    let window: gtk::Window = builder.object("main_window").unwrap();

    // set layer shell window
    set_layer_shell(&window);
    // set transparency window
    init_window(&window);

    window.set_application(Some(app));

    let main_context = glib::MainContext::default();
    main_context.spawn_local(async move {
        log::info!("Async Start");

        // start init view
        let button: gtk::Button = builder.object("update_weather").unwrap();
        let controller = Rc::new(RefCell::new(LocationController::new(builder)));
        init_controller(&controller);
        
        // set update data
        let controller_copy = controller.clone();
        button.connect_clicked(move |_| {
            log::info!("Click button and update data");
            controller_copy.borrow_mut().update_all();
        });
        loop {
            log::info!("Start update controller");
            controller.borrow_mut().update_all();
            timeout_future_seconds(60 * 30).await;
        }
    });

    window.show_all();
}

fn init_controller(controller: &Rc<RefCell<LocationController>>) {
    let mut controller = controller.borrow_mut();
    controller.add_label("location", |m| m.location.to_string());
    controller.add_label("now_weather", |m| m.now_temp.now.text.to_string());
    controller.add_label("week", |m| m.time.now_week.to_string());
    controller.add_label("feels_like", |m| {
        format!("体感 {}℃", m.now_temp.now.feels_like)
    });
    controller.add_label("humidity", |m| format!("湿度 {}%", m.now_temp.now.humidity));
    controller.add_label("now_temp", |m| format!("{}℃", m.now_temp.now.temp));
    controller.add_label("update_time", |m| {
        if m.success {
            format!("时间 {}", m.time.update_time)
        } else {
            "更新失败".to_string()
        }
    });

    controller.add_label("temp_day1", |m| {
        format!(
            "{}℃ ~ {}℃",
            m.day_temp.daily[0].temp_max, m.day_temp.daily[0].temp_min
        )
    });
    controller.add_label("temp_day2", |m| {
        format!(
            "{}℃ ~ {}℃",
            m.day_temp.daily[1].temp_max, m.day_temp.daily[1].temp_min
        )
    });
    controller.add_label("temp_day3", |m| {
        format!(
            "{}℃ ~ {}℃",
            m.day_temp.daily[2].temp_max, m.day_temp.daily[2].temp_min
        )
    });

    controller.add_label("day1", |m| m.time.day1_week.to_string());
    controller.add_label("day2", |m| m.time.day2_week.to_string());
    controller.add_label("day3", |m| m.time.day3_week.to_string());

    controller.add_image("now_icon", 60, |m| {
        format!("{}/icons/{}.svg", RESOURCE_PREFIX, m.now_temp.now.icon)
    });
    controller.add_image("icon_day1", 30, |m| {
        let mut icon = m.day_temp.daily[0].icon_day;
        if m.time.night {
            icon = m.day_temp.daily[0].icon_night;
        }
        format!("{}/icons/{}.svg", RESOURCE_PREFIX, icon)
    });
    controller.add_image("icon_day2", 30, |m| {
        let mut icon = m.day_temp.daily[1].icon_day;
        if m.time.night {
            icon = m.day_temp.daily[1].icon_night;
        }
        format!("{}/icons/{}.svg", RESOURCE_PREFIX, icon)
    });
    controller.add_image("icon_day3", 30, |m| {
        let mut icon = m.day_temp.daily[2].icon_day;
        if m.time.night {
            icon = m.day_temp.daily[2].icon_night;
        }
        format!("{}/icons/{}.svg", RESOURCE_PREFIX, icon)
    });

}

fn init_window(win: &gtk::Window) {
    set_visual(win, None);
    win.connect_screen_changed(set_visual);
    win.connect_draw(draw);
    win.set_app_paintable(true);
}

fn set_visual(window: &gtk::Window, _screen: Option<&gdk::Screen>) {
    if let Some(screen) = window.screen() {
        if let Some(ref visual) = screen.rgba_visual() {
            window.set_visual(Some(visual)); // crucial for transparency
        }
    }
}

fn draw(_window: &gtk::Window, ctx: &cairo::Context) -> Inhibit {
    // crucial for transparency
    ctx.set_source_rgba(1.0, 1.0, 1.0, 0.0);
    ctx.set_operator(cairo::Operator::Screen);
    ctx.paint().expect("Invalid cairo surface state");
    Inhibit(false)
}

fn set_layer_shell(win: &gtk::Window) {
    gtk_layer_shell::init_for_window(win);
    gtk_layer_shell::set_layer(win, gtk_layer_shell::Layer::Bottom);

    // set window right&top
    gtk_layer_shell::set_anchor(win, gtk_layer_shell::Edge::Right, true);
    gtk_layer_shell::set_anchor(win, gtk_layer_shell::Edge::Top, true);

    // set window margin
    gtk_layer_shell::set_margin(win, gtk_layer_shell::Edge::Right, 50);
    gtk_layer_shell::set_margin(win, gtk_layer_shell::Edge::Top, 50);
}
